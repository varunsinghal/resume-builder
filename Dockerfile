# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster
RUN apt-get update


WORKDIR /app
COPY . /app

EXPOSE 9010

RUN pip3 install poetry
RUN poetry install 