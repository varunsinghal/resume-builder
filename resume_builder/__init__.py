import logging
import os
from http import HTTPStatus

from flask import Flask, request

logger = logging.getLogger(__name__)


def resume_builder_cmd(username: str, file_id: str):
    logger.debug(f"resume builder for {username} - {file_id}.txt")


APPLICATION_ROOT = "/api"


def create_app(test_config=None):
    app = Flask(__name__, template_folder="../templates")
    app.config.from_pyfile(
        os.path.dirname(os.path.dirname(__file__)) + os.path.sep + "config.py"
    )

    if test_config:
        app.config.update(test_config)

    APPLICATION_ROOT = app.config["APPLICATION_ROOT"]

    @app.teardown_request
    def remove_session(ex=None):
        pass

    @app.route(APPLICATION_ROOT)
    def app_index():
        req_data = {
            "Endpoint": request.endpoint,
            "Method": request.method,
            "Cookies": request.cookies.to_dict(),
            "Args": request.args.to_dict(),
            "Remote": request.remote_addr,
        }
        req_data.update(dict(request.headers))
        return req_data

    @app.before_request
    def before_request():
        logging.info("Request ::: %r" % request.url)

    @app.after_request
    def after_request(response):
        response.headers.add("Access-Control-Allow-Origin", "*")
        response.headers.add(
            "Access-Control-Allow-Headers", "Content-Type, Authorization"
        )
        response.headers.add(
            "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS"
        )
        return response

    @app.errorhandler(Exception)
    def all_exception_handler(error):
        error_message = getattr(error, "message", str(error))
        logging.getLogger("root").error(error_message, exc_info=True)
        response = {
            "status": "error",
            "code": HTTPStatus.INTERNAL_SERVER_ERROR,
            "data": [],
            "message": "Something went wrong. "
            "Please contact the site admin.",
        }
        if app.config["DEBUG"]:
            response["data"] = error_message
        return response, HTTPStatus.INTERNAL_SERVER_ERROR

    @app.errorhandler(404)
    def api_not_found(error):
        error_message = getattr(error, "message", str(error))
        logging.getLogger("root").error(error_message, exc_info=True)
        return (
            {
                "status": "error",
                "code": HTTPStatus.NOT_FOUND,
                "data": {},
                "message": "API does not exist",
            },
            HTTPStatus.NOT_FOUND,
        )

    from .routes.export import export

    app.register_blueprint(
        export, url_prefix=APPLICATION_ROOT + "/v1/export/<user_id>"
    )

    return app


__all__ = [
    "resume_builder_cmd",
]
