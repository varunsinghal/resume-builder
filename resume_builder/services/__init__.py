import logging

from resume_builder.commons.database import Database


class Service:
    def __init__(
        self,
        user_id: str,
    ) -> None:
        self.log = logging.getLogger(__class__.__name__)
        self.db = Database(user_id)
        self.user_id = user_id
