from resume_builder.commons.models import Resume
from resume_builder.commons.serializer import ResumeSerializer
from resume_builder.commons.transformer import (
    AchievementTransformer,
    CertificationTransformer,
    ContactInfoTransformer,
    EducationTransformer,
    Html,
    ProfessionalSummaryTransformer,
    SkillsTransformer,
    TargetTitleTransformer,
    WorkExperienceTransformer,
)
from resume_builder.services import Service


class ResumeService(Service):
    map = {
        "contact_info": ContactInfoTransformer,
        "professional_summaries": ProfessionalSummaryTransformer,
        "work_experiences": WorkExperienceTransformer,
        "educations": EducationTransformer,
        "certifications": CertificationTransformer,
        "skills": SkillsTransformer,
        "achievements": AchievementTransformer,
        "target_titles": TargetTitleTransformer,
    }

    def get_html(self, file_id: str) -> str:
        serializer = ResumeSerializer()
        resume: Resume = serializer.load(self.db.query(file_id))
        commands = []
        for each_section in resume.get_section_order():
            commands.append(
                ResumeService.map.get(each_section)(
                    getattr(resume, each_section)
                )
            )
        return Html().render(resume.get_template_name(), commands)

    def get_resume(self, file_id: str) -> Resume:
        serializer = ResumeSerializer()
        resume: Resume = serializer.load(self.db.query(file_id))
        resume.group_experiences()
        return resume
