from flask import Blueprint, render_template, request

from resume_builder.services.resume import ResumeService

export = Blueprint("extract", __name__)


@export.route("/html", methods=["GET"])
def html_resume(user_id: str):
    file_id = request.args.get("file_id")
    service = ResumeService(user_id)
    resume = service.get_html(file_id)
    return render_template("template_1_base.html", resume=resume)
