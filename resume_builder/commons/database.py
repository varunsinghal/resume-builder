import json
import logging
import os

DATABASE_PATH = "./data/"
RECYCLE_BIN = "./recycle-bin/"


class Database:
    def __init__(self, user_id: str = "default-user"):
        self.log = logging.getLogger(self.__class__.__name__)
        self.base_path = DATABASE_PATH + user_id + os.path.sep
        self.recycle_bin = RECYCLE_BIN

    def query(self, file_id: str) -> dict:
        with open(self.base_path + file_id + ".json") as json_file:
            self.log.debug(
                "reading file -- %r" % self.base_path + file_id + ".json"
            )
            return json.loads(json_file.read())

    def save(self, file_id: str, obj):
        with open(self.base_path + file_id + ".json", "w") as json_file:
            self.log.debug(
                "saving file -- %r" % self.base_path + file_id + ".json"
            )
            return json_file.write(json.dumps(obj))

    def remove(self, file_id: str):
        self.log.debug(
            "removing file -- %r" % self.base_path + file_id + ".json"
        )
        os.replace(
            self.base_path + file_id + ".json",
            self.recycle_bin + file_id + ".json",
        )
