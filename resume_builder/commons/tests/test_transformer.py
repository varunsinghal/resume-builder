from unittest import TestCase

from ..factories import create_resume
from ..transformer import (
    AchievementTransformer,
    CertificationTransformer,
    ContactInfoTransformer,
    EducationTransformer,
    Html,
    ProfessionalSummaryTransformer,
    SkillsTransformer,
    TargetTitleTransformer,
    WorkExperienceTransformer,
)


class TestHtml(TestCase):
    def setUp(self) -> None:
        self.resume = create_resume()
        self.html = Html()

    def test_render_skills(self):
        html = self.html.render(
            "template_1", [SkillsTransformer(self.resume.skills)]
        )
        assert html is not None

    def test_render_contact_info(self):
        html = self.html.render(
            "template_1", [ContactInfoTransformer(self.resume.contact_info)]
        )
        assert html is not None

    def test_render_summary(self):
        html = self.html.render(
            "template_1",
            [
                ProfessionalSummaryTransformer(
                    self.resume.professional_summaries,
                )
            ],
        )
        assert html is not None

    def test_render_target_title(self):
        html = self.html.render(
            "template_1",
            [
                TargetTitleTransformer(
                    self.resume.target_titles,
                )
            ],
        )
        assert html is not None

    def test_render_work_experiences(self):
        html = self.html.render(
            "template_1",
            [WorkExperienceTransformer(self.resume.work_experiences)],
        )
        assert html is not None

    def test_render_education(self):
        html = self.html.render(
            "template_1", [EducationTransformer(self.resume.educations)]
        )
        assert html is not None

    def test_render_certification(self):
        html = self.html.render(
            "template_1", [CertificationTransformer(self.resume.certifications)]
        )
        assert html is not None

    def test_render_achievements(self):
        html = self.html.render(
            "template_1", [AchievementTransformer(self.resume.achievements)]
        )
        assert html is not None


class TestCommand(TestCase):
    def setUp(self) -> None:
        self.resume = create_resume()

    def test_prepare_skills(self):
        xml = SkillsTransformer(self.resume.skills).execute()
        for name in [skill.name for skill in self.resume.skills]:
            assert name in xml
        for info in [skill.info for skill in self.resume.skills]:
            assert info in xml

    def test_prepare_contact_info(self):
        xml = ContactInfoTransformer(self.resume.contact_info).execute()
        assert self.resume.contact_info.first_name in xml
        assert self.resume.contact_info.last_name in xml
        assert self.resume.contact_info.address in xml
        assert self.resume.contact_info.city in xml
        assert self.resume.contact_info.state in xml
        assert self.resume.contact_info.phone_number in xml
        assert self.resume.contact_info.email in xml
        assert self.resume.contact_info.linkedin in xml
        assert self.resume.contact_info.twitter in xml
        assert self.resume.contact_info.website in xml

    def test_prepare_summary(self):
        xml = ProfessionalSummaryTransformer(
            self.resume.professional_summaries
        ).execute()
        for summary in self.resume.professional_summaries:
            if summary.is_active:
                assert summary.text in xml

    def test_prepare_target_title(self):
        xml = TargetTitleTransformer(self.resume.target_titles).execute()
        for title in self.resume.target_titles:
            if title.is_active:
                assert title.text in xml

    def test_prepare_work_experience(self):
        xml = WorkExperienceTransformer(self.resume.work_experiences).execute()
        for work_exp in self.resume.work_experiences:
            assert work_exp.company_name in xml
            assert format(work_exp.start_date, "%m/%Y") in xml
            assert work_exp.position in xml
            if work_exp.end_date:
                assert format(work_exp.end_date, "%m/%Y") in xml
            else:
                assert "Present" in xml

    def test_prepare_education(self):
        xml = EducationTransformer(self.resume.educations).execute()
        for edu in self.resume.educations:
            assert edu.institution in xml
            assert edu.fields_of_study in xml

    def test_prepare_certification(self):
        xml = CertificationTransformer(self.resume.certifications).execute()
        for cert in self.resume.certifications:
            assert cert.name in xml
            assert format(cert.date, "%m/%Y") in xml
            assert cert.provider in xml

    def test_prepare_achievements(self):
        xml = AchievementTransformer(self.resume.achievements).execute()
        for achievement in self.resume.achievements:
            assert achievement.text in xml
