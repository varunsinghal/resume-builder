from ..factories import create_contact_info, create_resume
from ..serializer import ContactInfoSerializer, ResumeSerializer


def test_contact_info_serializer():
    contact_info = create_contact_info()
    serializer = ContactInfoSerializer()
    contact_info_dict = serializer.dump(contact_info)
    expected_keys = set(
        [
            "website",
            "last_name",
            "city",
            "linkedin",
            "email",
            "phone_number",
            "first_name",
            "state",
            "address",
            "twitter",
            "html",
        ]
    )
    assert set(contact_info_dict.keys()).difference(expected_keys) == set()


def test_resume_serializer():
    serializer = ResumeSerializer()
    resume = create_resume()
    resume_dict = serializer.dump(resume)
    expected_keys = set(
        [
            "target_titles",
            "achievements",
            "professional_summaries",
            "certifications",
            "skills",
            "educations",
            "contact_info",
            "id",
            "work_experiences",
            "template_setting",
        ]
    )
    assert set(resume_dict.keys()).difference(expected_keys) == set()
