from ..factories import (
    create_achievement,
    create_certification,
    create_contact_info,
    create_education,
    create_professional_summary,
    create_resume,
    create_skill,
    create_target_title,
    create_template_setting,
    create_work_experience,
)


def test_create_contact_info():
    contact_info = create_contact_info()
    assert contact_info is not None


def test_create_target_title():
    target_title = create_target_title()
    assert target_title is not None


def test_create_professional_summary():
    summary = create_professional_summary()
    assert summary is not None


def test_create_work_experience():
    work_exp = create_work_experience()
    assert work_exp is not None


def test_create_education():
    education = create_education()
    assert education is not None


def test_create_certification():
    certification = create_certification()
    assert certification is not None


def test_create_skill():
    skill = create_skill()
    assert skill is not None


def test_create_achievement():
    achievement = create_achievement()
    assert achievement is not None


def test_template_setting():
    setting = create_template_setting()
    assert setting is not None


def test_create_resume():
    resume = create_resume()
    assert resume is not None
