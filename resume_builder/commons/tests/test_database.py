from unittest import TestCase
from unittest.mock import call, patch

from resume_builder.commons.database import Database


class TestDatabase(TestCase):
    def setUp(self) -> None:
        self.mock_open = patch("resume_builder.commons.database.open").start()
        self.addCleanup(patch.stopall)

    def test_query(self):
        self.mock_open.return_value.__enter__.return_value.read.return_value = (
            '{"key": "value"}'
        )
        db = Database("user-id")
        assert db.query("file-id") == {"key": "value"}

    def test_save(self):
        db = Database("user-id")
        db.save("file-id", {"key": "value"})
        self.mock_open.assert_has_calls(
            [
                call("./data/user-id/file-id.json", "w"),
                call().__enter__(),
                call().__enter__().write('{"key": "value"}'),
                call().__exit__(None, None, None),
            ]
        )

    def test_remove(self):
        with patch("resume_builder.commons.database.os") as mock_os:
            db = Database("user-id")
            db.remove("file-id")
            mock_os.replace.assert_called_once()
