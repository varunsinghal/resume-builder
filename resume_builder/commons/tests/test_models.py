from resume_builder.commons.factories import create_resume
from resume_builder.commons.models import Resume


def test_grouped_experiences():
    resume: Resume = create_resume()
    resume.work_experiences[1].company_name = resume.work_experiences[
        0
    ].company_name
    resume.group_experiences()
    assert resume.work_experiences[1].is_grouped is True
    assert resume.work_experiences[0].is_grouped is False
    assert (
        resume.work_experiences[0].start_date
        == resume.work_experiences[1].start_date
    )
    for work_exp in resume.work_experiences[2:]:
        assert work_exp.is_grouped is False
