from .factories_impl import (
    AchievementFactory,
    CertificationFactory,
    ContactInfoFactory,
    EducationFactory,
    ProfessionalSummaryFactory,
    ResumeFactory,
    SkillFactory,
    TargetTitleFactory,
    TemplateSettingFactory,
    WorkExperienceFactory,
)
from .models import (
    Achievement,
    Certification,
    ContactInfo,
    Education,
    ProfessionalSummary,
    Resume,
    Skill,
    TargetTitle,
    TemplateSetting,
    WorkExperience,
)


def create_contact_info() -> ContactInfo:
    return ContactInfoFactory()


def create_target_title(**kwargs) -> TargetTitle:
    return TargetTitleFactory(**kwargs)


def create_professional_summary(**kwargs) -> ProfessionalSummary:
    return ProfessionalSummaryFactory(**kwargs)


def create_work_experience(**kwargs) -> WorkExperience:
    return WorkExperienceFactory(**kwargs)


def create_education(**kwargs) -> Education:
    return EducationFactory(**kwargs)


def create_certification(**kwargs) -> Certification:
    return CertificationFactory(**kwargs)


def create_skill(**kwargs) -> Skill:
    return SkillFactory(**kwargs)


def create_achievement(**kwargs) -> Achievement:
    return AchievementFactory(**kwargs)


def create_template_setting(**kwargs) -> TemplateSetting:
    return TemplateSettingFactory(**kwargs)


def create_resume(**kwargs) -> Resume:
    return ResumeFactory(**kwargs)
