from datetime import datetime, timedelta, timezone
from typing import List
from uuid import uuid4

import factory
import factory.fuzzy
from faker import Faker

from .models import (
    Achievement,
    Certification,
    ContactInfo,
    Education,
    JobType,
    ProfessionalSummary,
    Resume,
    Skill,
    TargetTitle,
    TemplateSetting,
    WorkExperience,
)


class TemplateSettingFactory(factory.Factory):
    class Meta:
        model = TemplateSetting

    template_name = "template_1"  # factory.Faker("word")

    @factory.lazy_attribute
    def section_order(self):
        section_order = [
            "contact_info",
            "professional_summaries",
            "work_experiences",
            "educations",
            "certifications",
            "skills",
            "achievements",
        ]
        factory.fuzzy.random.random.shuffle(section_order)
        return section_order

    def __repr__(self) -> str:
        return self.template_name


class ContactInfoFactory(factory.Factory):
    class Meta:
        model = ContactInfo

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    phone_number = factory.Faker("phone_number")
    address = factory.Faker("street_address")
    city = factory.Faker("city")
    state = factory.Faker("state")
    website = factory.Faker("url")

    @factory.lazy_attribute
    def linkedin(self):
        return f"https://linkedin.com/{Faker().pystr()}"

    @factory.lazy_attribute
    def twitter(self):
        return f"https://twitter.com/{Faker().pystr()}"


class TargetTitleFactory(factory.Factory):
    class Meta:
        model = TargetTitle

    text = factory.Faker("job")
    is_active = factory.fuzzy.FuzzyChoice([True, False])


class ProfessionalSummaryFactory(factory.Factory):
    class Meta:
        model = ProfessionalSummary

    text = factory.Faker("paragraph", nb_sentences=5)
    is_active = factory.fuzzy.FuzzyChoice([True, False])


class WorkExperienceFactory(factory.Factory):
    class Meta:
        model = WorkExperience

    class Params:
        duration = factory.fuzzy.FuzzyInteger(low=366, high=1700)

    company_name = factory.Faker("company")
    position = factory.Faker("job")
    location = factory.Faker("state")
    job_type = factory.fuzzy.FuzzyChoice([e for e in JobType])
    start_date = factory.fuzzy.FuzzyDateTime(
        start_dt=datetime(2008, 1, 1, tzinfo=timezone.utc)
    )
    achievements = factory.Faker("paragraphs")

    @factory.lazy_attribute
    def end_date(self):
        return self.start_date + timedelta(days=self.duration)


class EducationFactory(factory.Factory):
    class Meta:
        model = Education

    class Params:
        duration = factory.fuzzy.FuzzyInteger(low=366, high=1200)

    institution = factory.Faker("word")
    fields_of_study = factory.Faker("sentence")
    degree = factory.Faker("sentence", nb_words=2)
    location = factory.Faker("state")
    start_date = factory.fuzzy.FuzzyDateTime(
        start_dt=datetime(2008, 1, 1, tzinfo=timezone.utc)
    )
    achievements = factory.Faker("paragraphs")

    @factory.lazy_attribute
    def end_date(self):
        return self.start_date + timedelta(days=self.duration)

    @factory.post_generation
    def post(obj, create, extracted, **kwargs):
        obj.degree = obj.degree.strip(".")
        obj.fields_of_study = obj.fields_of_study.strip(".")


class CertificationFactory(factory.Factory):
    class Meta:
        model = Certification

    name = factory.Faker("sentence", nb_words=2)
    provider = factory.Faker("company")
    date = factory.fuzzy.FuzzyDateTime(
        start_dt=datetime(2008, 1, 1, tzinfo=timezone.utc)
    )

    @factory.post_generation
    def post(obj, create, extracted, **kwargs):
        obj.name = obj.name.strip(".")


class SkillFactory(factory.Factory):
    class Meta:
        model = Skill

    name = factory.Faker("word")
    info = factory.Faker("sentence")


class AchievementFactory(factory.Factory):
    class Meta:
        model = Achievement

    text = factory.Faker("sentence")


class ResumeFactory(factory.Factory):
    class Meta:
        model = Resume

    id = uuid4()
    template_setting = factory.SubFactory(TemplateSettingFactory)
    contact_info = factory.SubFactory(ContactInfoFactory)
    target_titles = factory.List(
        [
            factory.SubFactory(TargetTitleFactory, is_active=False)
            for _ in range(2)
        ]
    )
    professional_summaries = factory.List(
        [factory.SubFactory(ProfessionalSummaryFactory, is_active=False)]
    )

    @factory.lazy_attribute
    def educations(self) -> List[Education]:
        output = []
        begin_date = datetime(2000, 1, 1, tzinfo=timezone.utc)
        for _ in range(Faker().random_int(min=1, max=3)):
            education = EducationFactory(start_date=begin_date)
            begin_date = education.end_date
            output.insert(0, education)
        return output

    @factory.lazy_attribute
    def work_experiences(self) -> List[WorkExperience]:
        output = []
        begin_date = datetime(2008, 1, 1, tzinfo=timezone.utc)
        for _ in range(Faker().random_int(min=3, max=5)):
            work_exp = WorkExperienceFactory(start_date=begin_date)
            begin_date = work_exp.end_date
            output.insert(0, work_exp)
        return output

    certifications = factory.List(
        [factory.SubFactory(CertificationFactory) for _ in range(5)]
    )
    skills = factory.List([factory.SubFactory(SkillFactory) for _ in range(8)])
    achievements = factory.List(
        [factory.SubFactory(AchievementFactory) for _ in range(5)]
    )

    @factory.post_generation
    def post(obj, create, extracted, **kwargs):
        obj.target_titles[0].is_active = True
        obj.professional_summaries[0].is_active = True
        obj.work_experiences[0].end_date = None
