from marshmallow import EXCLUDE, Schema, fields, post_load

from .models import (
    Achievement,
    Certification,
    ContactInfo,
    Education,
    JobType,
    ProfessionalSummary,
    Resume,
    Skill,
    TargetTitle,
    TemplateSetting,
    WorkExperience,
)


class ContactInfoSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    first_name = fields.Str()
    last_name = fields.Str()
    email = fields.Email()
    phone_number = fields.Str(allow_none=True)
    linkedin = fields.Url(allow_none=True)
    twitter = fields.Url(allow_none=True)
    address = fields.Str(allow_none=True)
    city = fields.Str(allow_none=True)
    state = fields.Str(allow_none=True)
    website = fields.Url(allow_none=True)
    html = fields.Str(dump_only=True)

    @post_load
    def make_contact_info(self, data, **kwargs) -> ContactInfo:
        return ContactInfo(**data)


class TargetTitleSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Str()
    is_active = fields.Bool()

    @post_load
    def make_target_title(self, data, **kwargs) -> TargetTitle:
        return TargetTitle(**data)


class ProfessionalSummarySerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Str()
    is_active = fields.Bool()

    @post_load
    def make_professional_summary(self, data, **kwargs) -> ProfessionalSummary:
        return ProfessionalSummary(**data)


class WorkExperienceSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    company_name = fields.Str()
    position = fields.Str()
    location = fields.Str()
    job_type = fields.Enum(JobType, by_value=True)
    start_date = fields.Date(format="%m/%Y")
    end_date = fields.Date(format="%m/%Y", allow_none=True)
    is_present = fields.Bool(dump_only=True)
    achievements = fields.List(fields.Str())
    is_grouped = fields.Bool(dump_only=True)

    @post_load
    def make_work_experience(self, data, **kwargs) -> WorkExperience:
        return WorkExperience(**data)


class EducationSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    institution = fields.Str()
    fields_of_study = fields.Str()
    degree = fields.Str()
    location = fields.Str()
    start_date = fields.Date(format="%m/%Y")
    end_date = fields.Date(format="%m/%Y")
    achievements = fields.List(fields.Str())

    @post_load
    def make_education_serializer(self, data, **kwargs) -> Education:
        return Education(**data)


class CertificationSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    name = fields.Str()
    provider = fields.Str()
    date = fields.Date(format="%m/%Y")

    @post_load
    def make_certification(self, data, **kwargs) -> Certification:
        return Certification(**data)


class SkillSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    name = fields.Str()
    info = fields.Str()

    @post_load
    def make_skill(self, data, **kwargs) -> Skill:
        return Skill(**data)


class AchievementSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    text = fields.Str()

    @post_load
    def make_achievement(self, data, **kwargs) -> Achievement:
        return Achievement(**data)


class TemplateSettingSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    template_name = fields.Str()
    section_order = fields.List(fields.Str())

    @post_load
    def make_template_setting(self, data, **kwargs) -> TemplateSetting:
        return TemplateSetting(**data)


class ResumeSerializer(Schema):
    class Meta:
        unknown = EXCLUDE

    id = fields.Str()
    template_setting = fields.Nested(TemplateSettingSerializer)
    contact_info = fields.Nested(ContactInfoSerializer)
    target_titles = fields.List(fields.Nested(TargetTitleSerializer))
    professional_summaries = fields.List(
        fields.Nested(ProfessionalSummarySerializer)
    )
    work_experiences = fields.List(fields.Nested(WorkExperienceSerializer))
    educations = fields.List(fields.Nested(EducationSerializer))
    certifications = fields.List(fields.Nested(CertificationSerializer))
    skills = fields.List(fields.Nested(SkillSerializer))
    achievements = fields.List(fields.Nested(AchievementSerializer))

    @post_load
    def make_resume(self, data, **kwargs) -> Resume:
        return Resume(**data)
