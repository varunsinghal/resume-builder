from abc import abstractmethod
from typing import List

import lxml.etree as ET

from .models import (
    Achievement,
    Certification,
    ContactInfo,
    Education,
    ProfessionalSummary,
    Skill,
    TargetTitle,
    WorkExperience,
)


class Command:
    template: str

    @abstractmethod
    def execute(self) -> str:
        pass


class Html:
    def render(self, template_name: str, commands: List[Command]) -> str:
        html = ""
        for command in commands:
            xml = command.execute().encode()
            dom = ET.fromstring(xml)
            xslt = ET.parse(f"templates/{template_name}/{command.template}")
            html += ET.tostring(ET.XSLT(xslt)(dom)).decode()
        return html


class SkillsTransformer(Command):
    def __init__(self, skills: List[Skill]) -> None:
        self.skills = skills
        self.template = "skills.xsl"

    def execute(self):
        xml = "<skills>"
        for skill in self.skills:
            xml += "<skill>"
            xml += f"<name>{skill.name}</name>"
            xml += f"<info>{skill.info}</info>"
            xml += "</skill>"
        xml += "</skills>"
        return xml


class EducationTransformer(Command):
    def __init__(self, educations: List[Education]) -> None:
        self.educations = educations
        self.template = "education.xsl"

    def execute(self) -> str:
        xml = "<educations>"
        for edu in self.educations:
            xml += "<education>"
            xml += f"<institute-name>{edu.institution}</institute-name>"
            xml += f"<degree>{edu.degree}</degree>"
            xml += f"<field>{edu.fields_of_study}</field>"
            if edu.location:
                xml += f"<location>{edu.location}</location>"
            xml += f"<start-date>{format(edu.start_date, '%m/%Y')}</start-date>"
            xml += f"<end-date>{format(edu.end_date, '%m/%Y')}</end-date>"
            xml += "<achievements>"
            for text in edu.achievements:
                xml += "<achievement>"
                xml += f"<text>{text}</text>"
                xml += "</achievement>"
            xml += "</achievements>"
            xml += "</education>"
        xml += "</educations>"
        return xml


class CertificationTransformer(Command):
    def __init__(self, certifications: List[Certification]) -> None:
        self.certifications = certifications
        self.template = "certification.xsl"

    def execute(self) -> str:
        xml = "<certifications>"
        for certification in self.certifications:
            xml += "<certificate>"
            xml += f"<name>{certification.name}</name>"
            xml += f"<provider>{certification.provider}</provider>"
            xml += f"<date>{format(certification.date, '%m/%Y')}</date>"
            xml += "</certificate>"
        xml += "</certifications>"
        return xml


class AchievementTransformer(Command):
    def __init__(self, achievements: List[Achievement]) -> None:
        self.achievements = achievements
        self.template = "achievement.xsl"

    def execute(self) -> str:
        xml = "<achievements>"
        for achievement in self.achievements:
            xml += "<achievement>"
            xml += f"<text>{achievement.text}</text>"
            xml += "</achievement>"
        xml += "</achievements>"
        return xml


class ProfessionalSummaryTransformer(Command):
    def __init__(
        self,
        summaries: List[ProfessionalSummary],
    ) -> None:
        self.summaries = summaries
        self.template = "summary.xsl"

    def execute(self):
        xml = ""
        for summary in self.summaries:
            if summary.is_active:
                xml += "<summary>"
                xml += summary.text
                xml += "</summary>"
        return xml


class TargetTitleTransformer(Command):
    def __init__(
        self,
        target_titles: List[TargetTitle],
    ) -> None:
        self.target_titles = target_titles
        self.template = "target_title.xsl"

    def execute(self) -> str:
        xml = ""
        for title in self.target_titles:
            if title.is_active:
                xml += "<target-title>"
                xml += title.text
                xml += "</target-title>"
        return xml


class WorkExperienceTransformer(Command):
    def __init__(self, experiences: List[WorkExperience]) -> None:
        self.experiences = experiences
        self.template = "work_experience.xsl"

    def execute(self) -> str:
        xml = "<experiences>"
        for work in self.experiences:
            xml += "<work>"
            if not work.is_grouped:
                xml += f"<company-name>{work.company_name}</company-name>"
                xml += f"<location>{work.location}</location>"
                xml += (
                    "<start-date>"
                    f"{format(work.start_date, '%m/%Y')}"
                    "</start-date>"
                )
                if work.end_date:
                    xml += (
                        f"<end-date>{format(work.end_date, '%m/%Y')}</end-date>"
                    )
                else:
                    xml += "<end-date>Present</end-date>"
            xml += f"<position>{work.position}</position>"
            xml += "<achievements>"
            for text in work.achievements:
                xml += "<achievement>"
                xml += f"<text>{text}</text>"
                xml += "</achievement>"
            xml += "</achievements>"
            xml += "</work>"
        xml += "</experiences>"
        return xml


class ContactInfoTransformer(Command):
    def __init__(self, contact_info: ContactInfo) -> None:
        self.contact_info = contact_info
        self.template = "contact_info.xsl"

    def execute(self):
        address = ", ".join(
            filter(
                lambda v: v is not None,
                [
                    self.contact_info.address,
                    self.contact_info.city,
                    self.contact_info.state,
                ],
            )
        )
        xml = "<contactinfo>"
        xml += f"<firstname>{self.contact_info.first_name}</firstname>"
        xml += f"<lastname>{self.contact_info.last_name}</lastname>"
        if address:
            xml += f"<address>{address}</address>"
        if self.contact_info.phone_number:
            xml += f"<phone>{self.contact_info.phone_number}</phone>"
        if self.contact_info.email:
            xml += f"<email>{self.contact_info.email}</email>"
        if self.contact_info.linkedin:
            xml += "<linkedin>" f"{self.contact_info.linkedin}" "</linkedin>"
        if self.contact_info.twitter:
            xml += "<twitter>" f"{self.contact_info.twitter}" "</twitter>"
        if self.contact_info.website:
            xml += "<website>" f"{self.contact_info.website}" "</website>"
        xml += "</contactinfo>"
        return xml
