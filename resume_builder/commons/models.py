from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import List, Optional


class JobType(Enum):
    full_time = "Full-time"
    part_time = "Part-time"
    internship = "Internship"
    teaching = "Teaching"
    board = "Board"


@dataclass
class ContactInfo:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    address: str
    city: str
    state: str
    website: str
    linkedin: str
    twitter: str

    def __post_init__(self):
        if self.linkedin:
            self.linkedin = self.linkedin.strip("https://")
        if self.twitter:
            self.twitter = self.twitter.strip("https://")
        if self.website:
            self.website = self.website.strip("https://")


@dataclass
class TargetTitle:
    text: str
    is_active: bool


@dataclass
class ProfessionalSummary:
    text: str
    is_active: bool


@dataclass
class WorkExperience:
    company_name: str
    position: str
    location: str
    job_type: JobType
    start_date: datetime
    end_date: Optional[datetime]
    achievements: List[str]
    is_grouped: bool = False

    @property
    def is_present(self) -> bool:
        if not self.end_date:
            return True
        return False


@dataclass
class Education:
    institution: str
    fields_of_study: str
    degree: str
    location: str
    start_date: datetime
    end_date: Optional[datetime]
    achievements: List[str]


@dataclass
class Certification:
    name: str
    provider: str
    date: datetime


@dataclass
class Skill:
    name: str
    info: str


@dataclass
class Achievement:
    text: str


@dataclass
class TemplateSetting:
    template_name: str
    section_order: List[str]


@dataclass
class Resume:
    id: str
    template_setting: TemplateSetting
    contact_info: ContactInfo
    target_titles: List[TargetTitle]
    professional_summaries: List[ProfessionalSummary]
    work_experiences: List[WorkExperience]
    educations: List[Education]
    certifications: List[Certification]
    skills: List[Skill]
    achievements: List[Achievement]

    def group_experiences(self):
        current_work_exp = None
        for work_exp in self.work_experiences:
            if current_work_exp:
                if work_exp.company_name == current_work_exp.company_name:
                    work_exp.is_grouped = True
                    current_work_exp.start_date = work_exp.start_date
                else:
                    current_work_exp = work_exp
            else:
                current_work_exp = work_exp

    def get_section_order(self):
        return self.template_setting.section_order

    def get_template_name(self):
        return self.template_setting.template_name
