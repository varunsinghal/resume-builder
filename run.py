import logging
import os
from uuid import uuid4

import click

from resume_builder import resume_builder_cmd


@click.group(name="cli")
def cli():
    pass


@cli.command(name="resume", help="resume builder")
@click.option("--user", required=True)
@click.option("--file_id", required=True)
def resume_builder_cli(user: str, file_id: str):
    resume_builder_cmd(user, file_id)


if __name__ == "__main__":
    request_id = uuid4().hex
    logging.basicConfig(
        level=logging.DEBUG,
        handlers=[
            logging.FileHandler(f"logs{os.path.sep}{request_id}.log"),
            logging.StreamHandler(),
        ],
        format=(
            "[%(asctime)s][%(levelname)s][%(name)s.%(funcName)s]"
            f"[{request_id}] %(message)s"
        ),
    )
    logging.getLogger("faker").setLevel(logging.CRITICAL)
    logging.getLogger("factory").setLevel(logging.CRITICAL)
    cli()
