from http import HTTPStatus

from tests.routes import TestRoutes


class TestExportRoutes(TestRoutes):
    def setUp(self) -> None:
        super().setUp()
        self.user_id = "default-user"

    def test_export(self):
        actual = self.client.get(
            f"/dev/api/v1/export/{self.user_id}/html",
            headers=self.headers,
            query_string=dict(file_id="44d0f7dd-bf83-4011-a2a0-a070b8194bab"),
        )
        self.assertEqual(HTTPStatus.OK.value, actual.status_code)
        self.assertIn(b"work", actual.data)
