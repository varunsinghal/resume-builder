from unittest import TestCase

from resume_builder import create_app


class TestRoutes(TestCase):
    def setUp(self) -> None:
        app = create_app(
            {
                "TESTING": True,
                "SQLALCHEMY_DATABASE_URI": "",
                "DEBUG": True,
            }
        )
        self.headers = {}
        self.client = app.test_client()

    def tearDown(self) -> None:
        pass
