import logging
from logging.handlers import TimedRotatingFileHandler

from resume_builder import create_app

logging.basicConfig(
    level=logging.DEBUG,
    handlers=[
        TimedRotatingFileHandler(
            "logs/system.log", when="midnight", interval=1
        ),
        logging.StreamHandler(),
    ],
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
)
logging.getLogger("faker").setLevel(logging.CRITICAL)
logging.getLogger("factory").setLevel(logging.CRITICAL)


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=9010)
