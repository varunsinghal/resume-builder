<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div id="work-experience">
    <div class="section-header">
        WORK EXPERIENCE
    </div>
    <xsl:for-each select="experiences/work">
        <xsl:if test="company-name">
            <div>
                <span class="institute-name">
                    <xsl:value-of select="company-name"/>
                </span>
                &#x2022; <xsl:value-of select="location"/>
                &#x2022; <xsl:value-of select="start-date"/> -
                <xsl:value-of select="end-date"/>
            </div>
        </xsl:if>
        <div class="designation">
            <xsl:value-of select="position"/>
        </div>
        <div>
            <ul>
                <xsl:for-each select="achievements/achievement">
                    <li>
                        <span class="text">
                            <xsl:value-of select="text"/>
                        </span>
                    </li>
                </xsl:for-each>
            </ul>
        </div>
        <br/>
    </xsl:for-each>
</div><!-- #work-experience -->
</xsl:template>

</xsl:stylesheet>
