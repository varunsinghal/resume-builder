<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <div id="target-title" class="designation">
        <xsl:value-of select="target-title"/>
    </div> <!-- #targe-title -->
</xsl:template>

</xsl:stylesheet>
