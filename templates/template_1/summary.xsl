<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div>
    <div id="professional-summary" class="text">
        <xsl:value-of select="summary"/>
    </div><!-- #professional-summary -->
<br/>
</div>
</xsl:template>

</xsl:stylesheet>
