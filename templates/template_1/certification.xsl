<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div id="certification">
    <div class="section-header">CERTIFICATIONS</div>
    <xsl:for-each select="certifications/certificate">
    <div>
        <div class="designation">
            <xsl:value-of select="name"/>
        </div>
        <div class="text">
            <xsl:value-of select="provider"/> &#x2022; <xsl:value-of select="date"/>
        </div>
    </div>
    </xsl:for-each>
<br/>
</div><!-- #certifications -->
</xsl:template>

</xsl:stylesheet>
