<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div id="skills">
    <div class="section-header">SKILLS</div>
    <xsl:for-each select="skills/skill">
        <div class="text">
            <xsl:value-of select="name"/> - <xsl:value-of select="info"/>
        </div>
    </xsl:for-each>
<br/>
</div><!-- #skills -->
</xsl:template>

</xsl:stylesheet>
