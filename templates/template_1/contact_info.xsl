<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div>
    <div id="full-name" class="page-title">
        <xsl:value-of select="contactinfo/firstname"/>&#160;<xsl:value-of select="contactinfo/lastname"/>
    </div>
    <div id="contact-info" class="text">
        <xsl:if test="contactinfo/address">
            <span><xsl:value-of select="contactinfo/address"/>&#160;</span>
        </xsl:if>
        <xsl:if test="contactinfo/phone">
            <span>&#x2022;&#160;<xsl:value-of select="contactinfo/phone"/>&#160;</span>
        </xsl:if>
        <xsl:if test="contactinfo/email">
            <span>&#x2022;&#160;<xsl:value-of select="contactinfo/email"/>&#160;</span>
        </xsl:if>
        <xsl:if test="contactinfo/linkedin">
            <span>&#x2022;&#160;<xsl:value-of select="contactinfo/linkedin"/>&#160;</span>
        </xsl:if>
        <xsl:if test="contactinfo/twitter">
            <span>&#x2022;&#160;<xsl:value-of select="contactinfo/twitter"/>&#160;</span>
        </xsl:if>
        <xsl:if test="contactinfo/website">
            <span>&#x2022;&#160;<xsl:value-of select="contactinfo/website"/>&#160;</span>
        </xsl:if>
    </div><!-- #contact-info -->
<br/>
</div>
</xsl:template>

</xsl:stylesheet>
