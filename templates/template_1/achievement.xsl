<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div id="achievements">
    <div class="section-header">ACHIEVEMENTS</div>
    <ul>
        <xsl:for-each select="achievements/achievement">
        <li>
            <span class="text"><xsl:value-of select="text"/></span>
        </li>
        </xsl:for-each>
    </ul>
</div><!-- #achievemnts -->
</xsl:template>

</xsl:stylesheet>
