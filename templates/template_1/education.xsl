<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<div id="education">
    <div class="section-header">
        EDUCATION
    </div>
    <xsl:for-each select="educations/education">
    <div class="institute-name">
        <xsl:value-of select="institute-name"/>
    </div>
    <span class="designation">
        <xsl:value-of select="degree"/>
    </span> in <xsl:value-of select="field"/>
    <xsl:if test="location">
        &#x2022; <xsl:value-of select="location"/>
    </xsl:if>
    &#x2022; <xsl:value-of select="start-date"/> - <xsl:value-of select="end-date"/>
    <ul>
        <xsl:for-each select="achievements/achievement">
        <li>
            <span class="text">
                <xsl:value-of select="text"/>
            </span>
        </li>
        </xsl:for-each>
    </ul>
    <br/>
    </xsl:for-each>
<br/>
</div><!-- #education -->
</xsl:template>

</xsl:stylesheet>
