start:
	docker compose up -d

bash:
	docker-compose exec app bash

test:
	docker-compose exec -T app poetry run pytest

lint:
	docker-compose exec -T app poetry run isort .
	docker-compose exec -T app poetry run black .
	docker-compose exec -T app poetry run flake8 .

lint_check:
	docker-compose exec -T app poetry run flake8
	docker-compose exec -T app poetry run black . --check
	docker-compose exec -T app poetry run isort . -c

serve:
	docker-compose exec -T app poetry run python app.py

stop:
	docker-compose stop

requirements:
	docker-compose exec -T app poetry export -f requirements.txt -o requirements.txt --without-hashes

whl:
	docker-compose exec -T app poetry build

install:
	docker-compose exec -T app poetry install

resume:
	docker-compose exec -T app poetry run python run.py resume --user=$(USER) --file_id=$(FILE) 

clean:
	docker-compose down --rmi local